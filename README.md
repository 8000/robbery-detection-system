# Robbery detection system

The main purpose of this is to identify a situation of robbery in shops especially in Jewelry shops and the banks. In Sri Lanka is has been noticed when the robbery happens they have covered their faces with full face helmets and the armed with the guns.

Through this system it identify the presences of the guns and full face helmets in the targeted shop. If there are any presence of them, then the automatic alarm system will be activated and inform the necessary parties about the robbery. The parties which must be informed such a situation will be clarify by the customer. 

The main part of process of implement this system is creating the neural network to identify the presence of the guns and the helmets. Among the different type of neural networks, convolution neural network will be used here and will be implement on the Tensorflow platform.
