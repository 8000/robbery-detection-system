from cv2 import *
import time

sleep_time=1.5    # in seconds
show_time=800   # in miliseconds
num_image=30    # number of images must be taken

cam =VideoCapture(0)
s, img =cam.read()
time.sleep(2)
for i in range(num_image):
    s, img =cam.read()
    if s:
        namedWindow("cam-test")
        imshow ("cam-test",img)
        waitKey(show_time)
        destroyWindow("cam-test")
        imwrite("gun/Gun."+str(i)+".jpg",img)
        time.sleep(sleep_time)
